<?php

use yii\db\Migration;

class m190531_064312_add_images_touser extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'images', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'images');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190531_064312_add_images_touser cannot be reverted.\n";

        return false;
    }
    */
}
