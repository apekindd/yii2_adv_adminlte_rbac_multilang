<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>


<div class="content-page">
    <div class="content">
        <div class="">
            <div class="page-header-title"><h4 class="page-title"><?php
                    if ($this->title !== null) {
                        echo \yii\helpers\Html::encode($this->title);
                    } else {
                        echo \yii\helpers\Inflector::camel2words(
                            \yii\helpers\Inflector::id2camel($this->context->module->id)
                        );
                        echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                    } ?></h4></div>

            <?= Alert::widget() ?>
        </div>
        <div class="page-content-wrapper ">
            <div class="container">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <?php /*
                            Breadcrumbs::widget(
                                [
                                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                ]
                            ) */ ?>
                            <h4 class="m-b-30 m-t-0"></h4>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?= $content ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">Developed by NextDoorCoders</footer>
</div>
