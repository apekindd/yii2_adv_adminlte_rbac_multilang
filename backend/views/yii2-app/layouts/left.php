<div class="left side-menu">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 818px;">
        <div class="sidebar-inner slimscrollleft" style="overflow: hidden; width: auto; height: 818px;">
            <div class="user-details">
                <div class="text-center"><img
                            src="./WebAdmin - Responsive Admin Dashboard Template_files/avatar-1.jpg" alt=""
                            class="img-circle"></div>
                <div class="user-info">
                    <div class="dropdown"><a
                                             class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Kenny
                            Rigdon</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)"> Profile</a></li>
                            <li><a href="javascript:void(0)"> Settings</a></li>
                            <li><a href="javascript:void(0)"> Lock screen</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:void(0)"> Logout</a></li>
                        </ul>
                    </div>
                    <p class="text-muted m-0"><i class="fa fa-dot-circle-o text-success"></i> Online</p></div>
            </div>
            <div id="sidebar-menu">

                <?= dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            [
                                'label' => 'Пользователи',
                                'icon' => 'fa fa-users',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Пользователи', 'icon' => 'fa fa-user-plus', 'url' => ['/user']],
                                    ['label' => 'Роли', 'icon' => 'fa fa-user-secret', 'url' => ['/role']],
                                    ['label' => 'Права', 'icon' => 'fa fa-key', 'url' => ['/permission']],
                                ],
                            ],
                            [
                                'label' => 'Статика',
                                'icon' => 'fa fa-folder-open',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Языковые сообщения', 'icon' => 'fa fa-comment', 'url' => ['/message']],
                                    ['label' => 'Хранилище файлов', 'icon' => 'fa fa-file', 'url' => ['/file']],
                                    ['label' => 'Служебные', 'icon' => 'fa fa-link', 'url' => ['/service']],
                                ],
                            ],

                            ['label' => 'Настройки', 'icon' => 'fa fa-cog', 'url' => ['/settings']],
                        ],
                    ]
                ) ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="slimScrollBar"
             style="background: rgb(187, 187, 187); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 818px; visibility: visible;"></div>
        <div class="slimScrollRail"
             style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
    </div>
</div>
