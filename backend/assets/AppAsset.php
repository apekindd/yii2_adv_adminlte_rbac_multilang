<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/morris.css',
        'css/bootstrap.min.css',
        'css/icons.css',
        'css/style.css',
        //'css/site.css',
    ];
    public $js = [
        //'js/jquery.min.js',
        'js/bootstrap.min.js',
        'js/modernizr.min.js',
        'js/detect.js',
        'js/fastclick.js',
        'js/jquery.slimscroll.js',
        'js/jquery.blockUI.js',
        'js/waves.js',
        'js/wow.min.js',
        'js/jquery.scrollTo.min.js',
        //'js/morris.min.js',
        'js/raphael-min.js',
        //'js/dashborad.js',
        'js/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
